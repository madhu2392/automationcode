Feature: Google search button verification

  @GoogleSearchBtn
  Scenario: Verify the presence of Google Search button
    Given I open the browser
    When Navigate to google home page
    Then I validate the presence of google search button
