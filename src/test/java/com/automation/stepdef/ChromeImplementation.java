package com.automation.stepdef;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChromeImplementation {
	Properties p;
	
	@FindBy(xpath="//div[@class='FPdoLc tfB0Bf']/center/input[@value='Google Search']")
	private WebElement googleSearchBtn;
	
	@Before
	public void loadProperties() throws IOException
	{
		FileInputStream fp=new FileInputStream("E:/AutomationCode/src/test/resources/Property/config.properties");
		p=new Properties();
		p.load(fp);
	}
	
	WebDriver driver;
	@Given("^I open the browser$")
	public void i_open_the_browser() throws Throwable {
		if(p.getProperty("runOnJenkins").equals("true"))
		{
			driver = new RemoteWebDriver(new URL("http://172.20.10.2:8080/wd/hub"),DesiredCapabilities.chrome());
		}
		else{
		System.setProperty("webdriver.chrome.driver", "C:/Users/Mahesh_2/Downloads/chromedriver.exe");
		 driver=new ChromeDriver();
		} 
	}

	@When("^Navigate to google home page$")
	public void navigate_to_google_home_page() throws Throwable {
		driver.get("https://www.google.com/");
		 driver.manage().window().maximize();
	}

	@Then("^I validate the presence of google search button$")
	public void i_validate_the_presence_of_google_search_button() throws Throwable {
		
		/*WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(googleSearchBtn)).isDisplayed();*/
		driver.findElement(By.xpath("//input[@name='q']")).sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		boolean gsbtn=driver.findElement(By.xpath("//div[@class='FPdoLc tfB0Bf']/center/input[@value='Google Search']")).isDisplayed();
		Assert.assertTrue("Google Search button is not available", gsbtn);
	   
	}


}
